<?php
/*
 *   *********************************************************************************************
 *      Please retain this copyright header in all versions of the software.
 *      Bitte belassen Sie diesen Copyright-Header in allen Versionen der Software.
 *
 *      Copyright (C) Josef A. Puckl | eComStyle.de
 *      All rights reserved - Alle Rechte vorbehalten
 *
 *      This commercial product must be properly licensed before being used!
 *      Please contact info@ecomstyle.de for more information.
 *
 *      Dieses kommerzielle Produkt muss vor der Verwendung ordnungsgemäß lizenziert werden!
 *      Bitte kontaktieren Sie info@ecomstyle.de für weitere Informationen.
 *   *********************************************************************************************
 */

$sMetadataVersion   = '2.0';
$aModule            = [
    'id'            => 'ecs_wkmenge',
    'title'         => '<strong style="color:#04B431;">e</strong><strong>ComStyle.de</strong>:  <i>WarenkorbMenge</i>',
    'description'   => 'Nach Aenderung der Artikelmenge mit den + und - Schaltflaechen wird der Warenkorb automatisch aktualisiert.',
    'version'       => '2.1.1',
    'thumbnail'     => 'ecs.png',
    'author'        => '<strong style="font-size: 17px;color:#04B431;">e</strong><strong style="font-size: 16px;">ComStyle.de</strong>',
    'email'         => 'info@ecomstyle.de',
    'url'           => 'https://ecomstyle.de',
    'extend' => [],
    'blocks' => [
        ['template' => 'page/checkout/inc/basketcontents_list.tpl', 'block' => 'checkout_basketcontents_basketitem_quantity', 'file' => '/views/blocks/basketcontents_list.tpl'],
    ],
    'events' => [
        'onActivate'   => 'Ecs\WarenkorbMenge\Core\Events::onActivate',
        'onDeactivate' => 'Ecs\WarenkorbMenge\Core\Events::onDeactivate',
    ],
];
