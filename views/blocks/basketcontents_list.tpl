[{*
  *   *********************************************************************************************
  *      Please retain this copyright header in all versions of the software.
  *      Bitte belassen Sie diesen Copyright-Header in allen Versionen der Software.
  *
  *      Copyright (C) Josef A. Puckl | eComStyle.de
  *      All rights reserved - Alle Rechte vorbehalten
  *
  *      This commercial product must be properly licensed before being used!
  *      Please contact info@ecomstyle.de for more information.
  *
  *      Dieses kommerzielle Produkt muss vor der Verwendung ordnungsgemäß lizenziert werden!
  *      Bitte kontaktieren Sie info@ecomstyle.de für weitere Informationen.
  *   *********************************************************************************************
  *}]

[{if $editable && !$basketitem->isDiscountArticle() }]
[{oxscript add="var a = $('input[name =\"aproducts[$basketindex][am]\"]');
    a.attr('type', 'number');
    a.on('change', function() {
    $( '#basket_form' ).submit();
    });
"}]
[{/if}]
[{$smarty.block.parent}]